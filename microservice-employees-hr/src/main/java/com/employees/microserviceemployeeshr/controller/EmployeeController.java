package com.employees.microserviceemployeeshr.controller;

import com.employees.microserviceemployeeshr.model.Employee;
import com.employees.microserviceemployeeshr.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public Page<Employee> getEmployees(Pageable pageable){
        return employeeRepository.findAll(pageable);
    }
}
