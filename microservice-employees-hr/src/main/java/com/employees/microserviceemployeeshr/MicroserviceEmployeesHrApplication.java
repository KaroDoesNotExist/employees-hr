package com.employees.microserviceemployeeshr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceEmployeesHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceEmployeesHrApplication.class, args);
	}

}
